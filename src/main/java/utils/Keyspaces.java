package utils;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SimpleStatement;
import java.util.logging.*;

public final class Keyspaces {
    
    public static void setupKeyspaces(Cluster cluster) {
        String keyspace = "CREATE KEYSPACE IF NOT EXISTS instagrim WITH REPLICATION = { 'class': 'SimpleStrategy', 'replication_factor': 1 }";
        Session session = cluster.connect();
        
        try {            
            // create keyspace
            PreparedStatement preparedStatement = session.prepare(keyspace);
            BoundStatement boundStatement = new BoundStatement(preparedStatement);
            ResultSet resultSet = session.execute(boundStatement);
            Logger.getLogger(Keyspaces.class.getName()).log(Level.INFO, "Created database");       
        } catch (Exception e) {
            Logger.getLogger(Keyspaces.class.getName()).log(Level.SEVERE, "Could not create database: " + e);            
        }
        
        // create various associated tables
        createUsers(session);
        createImages(session);
        
        session.close();
    }
    
    private static void createUsers(Session session) {
        String stub = "\n" +  
                "CREATE TABLE IF NOT EXISTS instagrim.users (\n" +
                "   username text PRIMARY KEY, \n" + 
                "   displayUsername text, \n" +
                "   password text, \n" +
                "   salt text\n" +
                ");";
        
        try {
            SimpleStatement query = new SimpleStatement(stub);
            session.execute(query);
        } catch (Exception e) {
            Logger.getLogger(Keyspaces.class.getName()).log(Level.SEVERE, "Could not create instagrim.users: " + e);
        }
    }
    
    private static void createImages(Session session) {
        String stub = "\n" +
                "CREATE TABLE IF NOT EXISTS instagrim.images (\n" +
                "   id text, \n" +
                "   uploaderUsername text, \n" +
                "   base64 text,\n" +
                "   timestamp bigint, \n" + 
                "   PRIMARY KEY(id, uploaderUsername, timestamp) \n" + // this is unintuitive.
                ") WITH CLUSTERING ORDER BY (uploaderUsername ASC, timestamp DESC);";
                
        try {
            SimpleStatement query = new SimpleStatement(stub);
            session.execute(query); 
        } catch (Exception e) {
            Logger.getLogger(Keyspaces.class.getName()).log(Level.SEVERE, "Could not create instagrim.images: " + e);            
        }
    }
}
