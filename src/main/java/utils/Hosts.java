package utils;

import com.datastax.driver.core.*;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.*;

public final class Hosts {
    private static Cluster cluster;
    private static final String HOST = "127.0.0.1";
    
    public static String getHost() {
        return HOST;
    }
    
    public static String[] getHosts(Cluster cluster) {
        if (cluster == null) {
            Logger.getLogger(Hosts.class.getName()).log(Level.INFO, "Creating cluster connection.");            
            cluster = Cluster.builder().addContactPoint(HOST).build();
        }
        
        Logger.getLogger(Hosts.class.getName()).log(Level.INFO, "Cluster Name: {0}", cluster.getClusterName());
        
        Metadata metadata = cluster.getMetadata();
        Set<Host> hosts = metadata.getAllHosts();
        String[] sHosts = new String[hosts.size()];
        
        Iterator<Host> it = hosts.iterator();
        int i = 0;
        while (it.hasNext()) {
            Host ch = it.next();
            sHosts[i] = ch.getAddress().toString();
            
            Logger.getLogger(Hosts.class.getName()).log(Level.INFO, "Hosts {0} ({1})", new Object[]{sHosts[i], ch.getSocketAddress().getPort()});
            i++;
        }
        
        return sHosts;
    }
    
    public static Cluster getCluster() {
        cluster = Cluster.builder().addContactPoint(HOST).build();
        
        if (getHosts(cluster) == null) {
            return null;
        }
        
        Keyspaces.setupKeyspaces(cluster);
        
        return cluster;
    }
    
    public static ResultSet query(Cluster clusterInstance, String query, Object... params) {
        // wrapper method for preparing statements. Less boilerplate = fun!
        Session session = clusterInstance.connect("instagrim");
        PreparedStatement preparedStatement = session.prepare(query);
        BoundStatement boundStatement = new BoundStatement(preparedStatement);
        ResultSet resultSet = session.execute(boundStatement.bind(params));
        return resultSet;
    }
    
    public void close() {
        cluster.close();
    }
}
