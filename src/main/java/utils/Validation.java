package utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Validation {
    public static String hashPassword(String password, String salt) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        // create SHA-256 instance
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        
        // append salt to password then hash.
        md.update((password + salt).getBytes("UTF-8"));
        byte[] digestedPassword1 = md.digest();

        // return as hex string.
        return String.format("%064x", new java.math.BigInteger(1, digestedPassword1));
    }
    
    public static boolean validatePassword(String inputPassword, String hashedPassword, String salt) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return (hashedPassword.equals(hashPassword(inputPassword, salt)));
    }
}
