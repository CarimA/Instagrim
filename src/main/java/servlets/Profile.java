package servlets;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.sun.istack.internal.logging.Logger;
import exceptions.NoInformationException;
import exceptions.PasswordInvalidException;
import exceptions.UsernameInvalidException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.*;

@WebServlet(name="Profile", urlPatterns={"/Profile/*"})
public class Profile extends HttpServlet {
    Cluster cluster = null;
    
    public void init(ServletConfig config) throws ServletException {
        cluster = Hosts.getCluster();
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getPathInfo().split("/")[1].toLowerCase();
        
        List<Row> exists = Hosts.query(cluster, "SELECT displayUsername, username FROM instagrim.users WHERE username = ?", id).all();
        if ((exists.isEmpty())) {
            request.getSession().setAttribute("error", "This user does not exist.");    
            response.sendRedirect("/Instagrim/Home"); 
            return;                        
        }        
        
        request.setAttribute("user", exists.get(0).getString("displayUsername"));
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/profile.jsp?id=" + id);
	requestDispatcher.forward(request, response);        
    }
}
