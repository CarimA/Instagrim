package servlets;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.time.Instant;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.List;
import java.util.UUID;
import java.util.zip.CRC32;
import java.util.zip.Checksum;
import javax.imageio.ImageIO;
import javax.naming.SizeLimitExceededException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import models.User;
import org.apache.commons.io.IOUtils;
import utils.Hosts;
import utils.Keyspaces;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Carim A
 */
@WebServlet(name = "Image", urlPatterns = { "/Image" })
@MultipartConfig(maxFileSize = 10485760, maxRequestSize = 10485760, fileSizeThreshold = 10485760)
public class Image extends HttpServlet {
    Cluster cluster = null;
    
    public void init(ServletConfig config) throws ServletException {
        cluster = Hosts.getCluster();
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String imageID = request.getParameter("id");
        String uploader = request.getParameter("uploader");
        
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        
        if ((imageID == null || imageID.equals("")) && (uploader == null || uploader.equals(""))) {
            // no ID or user was provided: assume we want everything, so give it in JSON
            // i do not like this. at all.
            ResultSet image = Hosts.query(cluster, "SELECT id FROM instagrim.images");
            out.print("[");
            int i = 0;
            List<Row> rows = image.all();
            for (Row r : rows) {
                out.print("\"" + r.getString("id") + "\"");
                i++;
                if (!(i == rows.size())) {
                    out.print(", ");
                }
            }
            out.print("]");
        } else {
            ResultSet images;
            if (!(imageID == null || imageID.equals("")) && !(uploader == null || uploader.equals(""))) {
                // we have both. build a query as such (even though this will never be used)
                images = Hosts.query(cluster, "SELECT base64 FROM instagrim.images WHERE id = ? AND uploaderUsername = ?", imageID, uploader);
            } else {
                if (imageID == null || imageID.equals("")) {
                    // we have uploader
                    uploader = uploader.toLowerCase();
                    images = Hosts.query(cluster, "SELECT id FROM instagrim.images WHERE uploaderUsername = ? ALLOW FILTERING", uploader);
                } else {
                    // we have ID
                    images = Hosts.query(cluster, "SELECT base64, uploaderUsername, timestamp FROM instagrim.images WHERE id = ?", imageID);
                }
            }
            
            out.print("[");
            int i = 0;
            List<Row> rows = images.all();
            for (Row r : rows) {
                if (imageID == null || imageID.equals("")) {
                    out.print("\"" + r.getString("id") + "\"");     
                } else {
                    out.print("\"" + r.getString("base64") + "\"");
                    out.print(", ");               
                    out.print("\"" + r.getString("uploaderUsername") + "\"");      
                    out.print(", ");               
                    out.print(r.getLong("timestamp"));  
                }
                i++;
                if (!(i == rows.size())) {
                    out.print(", ");
                }
            }   
            out.print("]");
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // check if user is logged in. If not, go away.
        if (request.getSession().getAttribute("session") == null) {
            request.getSession().setAttribute("error", "You must be logged in to use this functionality!");   
            response.sendRedirect("Login");
            return;
        }
        
        // debugging
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        //out.println(request);
        
        try {
            for (Part part : request.getParts()) {
                InputStream inputStream = request.getPart(part.getName()).getInputStream();               
                
                int i = inputStream.available();
                if (i > 0) {
                    byte[] image = new byte[i + 1];
                    inputStream.read(image);
                    
                    // first, check that it's actually an image.                    
                    // source: http://stackoverflow.com/a/4169776/6785373
                    try {
                        InputStream copy = new ByteArrayInputStream(image);
                        ImageIO.read(copy).toString();
                        // It's an image (only BMP, GIF, JPG and PNG are recognized).
                    } catch (Exception e) {
                        // It's not an image.
                        request.getSession().setAttribute("error", "You can only upload images!");    
                        response.sendRedirect(""); 
                        return;
                    } 
                    
                    // second, make sure it doesn't exist.
                    Checksum checksum = new CRC32();
                    checksum.update(image, 0, image.length);
                    long checksumValue = checksum.getValue();
                    
                    ResultSet exists = Hosts.query(cluster, "SELECT id FROM instagrim.images WHERE id = ?", (checksumValue + "").toString());
                    if (!(exists.all().isEmpty())) {
                        request.getSession().setAttribute("error", "This image has already been uploaded.");    
                        response.sendRedirect(""); 
                        return;                        
                    }

                    // finally, encode to base 64 and store in our database.
                    Encoder encoder = Base64.getEncoder();
                    String base64image = encoder.encodeToString(image);

                    inputStream.close();

                    // insert this string into images
                    Hosts.query(cluster, "INSERT INTO instagrim.images(id, uploaderUsername, base64, timestamp) VALUES (?, ?, ?, ?)", 
                            (checksumValue + "").toString(), ((User)request.getSession().getAttribute("session")).getUsername(), base64image, Instant.now().getEpochSecond());
                }
            }
            request.getSession().setAttribute("success", "Image uploaded!"); 
        } catch (Exception ex) {
            request.getSession().setAttribute("error", "Could not upload image. Your image cannot exceed 10MB.");             
        }
          
        response.sendRedirect("");
    }
}
