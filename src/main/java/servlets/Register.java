/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.datastax.driver.core.Cluster;
import exceptions.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.User;
import utils.*;

@WebServlet(name = "Register", urlPatterns = { "/Register" })
public class Register extends HttpServlet {
    Cluster cluster = null;
    
    public void init(ServletConfig config) throws ServletException {
        cluster = Hosts.getCluster();
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession().getAttribute("session") != null) {
            request.getSession().setAttribute("error", "You are already logged in!");   
            response.sendRedirect("Home");
            return;
        }
        
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String passwordConfirm = request.getParameter("password2");
        String redirect = "Home";
        
        try {
            User user = User.register(cluster, username, password, passwordConfirm);
            request.getSession().setAttribute("session", user);
            request.getSession().setAttribute("success", "You have successfully created an Instagrim account!");   
        } catch (NoInformationException niEx) {
            request.getSession().setAttribute("error", "Some information is missing. Please provide your desired username and password.");      
            redirect = "Login";
        } catch (UsernameTakenException utEx) {
            request.getSession().setAttribute("error", "This username is taken.");
            redirect = "login.jsp";        
        } catch (UsernameTooLongException utlEx) {
            request.getSession().setAttribute("error", "This username is too long.");
            redirect = "Login";                  
        } catch (PasswordsDoNotMatchException pdnmEx) {          
            request.getSession().setAttribute("error", "Your passwords do not match.");  
            redirect = "Login";
        } catch (PasswordRequiresUppercaseException pruEx) {          
            request.getSession().setAttribute("error", "Your password must contain at least one uppercase letter.");  
            redirect = "Login";
        } catch (PasswordRequiresLowercaseException prlEx) {
            request.getSession().setAttribute("error", "Your password must contain at least one lowercase letter.");
            redirect = "Login";
        } catch (PasswordRequiresDigitException prdEx) {          
            request.getSession().setAttribute("error", "Your password must contain at least one numerical digit.");  
            redirect = "Login";
        } catch (PasswordRequiresSixCharactersException prscEx) {         
            request.getSession().setAttribute("error", "Your password must be 6 or more characters.");   
            redirect = "Login";
        } catch (Exception ex) {
            // shouldn't ever reach here.         
            request.getSession().setAttribute("error", "An unknown error occured. Please try again later.");   
            redirect = "Login";
        } finally {
            //RequestDispatcher rd = request.getRequestDispatcher(redirect);
	    //rd.forward(request,response);      
            response.sendRedirect(redirect);
        }
    }
   
    @Override
    public String getServletInfo() {
        return "Handles POST requests for registration attempts.";
    }
    
}
