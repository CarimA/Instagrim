package servlets;

import com.datastax.driver.core.Cluster;
import exceptions.NoInformationException;
import exceptions.PasswordInvalidException;
import exceptions.UsernameInvalidException;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.User;
import utils.*;

@WebServlet(name="Login", urlPatterns={"/Login"})
public class Login extends HttpServlet {
    Cluster cluster = null;
    
    public void init(ServletConfig config) throws ServletException {
        cluster = Hosts.getCluster();
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
	requestDispatcher.forward(request, response);        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession().getAttribute("session") != null) {
            request.getSession().setAttribute("error", "You are already logged in!");   
            response.sendRedirect("Home");
            return;
        }
        
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        
        String redirect = "Home";
        try {
            User user = User.login(cluster, username, password);
            request.getSession().setAttribute("session", user);
            request.getSession().setAttribute("success", "You have successfully logged in to Instagrim!");   
        } catch (NoInformationException niEx) {
            request.getSession().setAttribute("error", "Some information is missing. Please provide your username and password.");      
            redirect = "Login";
        } catch (UsernameInvalidException | PasswordInvalidException uipiEx) {
            // for security reasons, we don't tell our user which information was incorrect, just that somethiing is wrong.
            request.getSession().setAttribute("error", "The username and/or password is invalid.");
            redirect = "Login";            
        } catch (Exception ex) {
            // shouldn't ever reach here.
            request.getSession().setAttribute("error", "An unknown error occured. Please try again later.");   
            redirect = "Login";
        } finally {
            response.sendRedirect(redirect);
        }
    }
}
