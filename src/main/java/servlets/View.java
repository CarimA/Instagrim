package servlets;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.sun.istack.internal.logging.Logger;
import exceptions.NoInformationException;
import exceptions.PasswordInvalidException;
import exceptions.UsernameInvalidException;
import java.io.IOException;
import java.util.logging.Level;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.*;

@WebServlet(name="View", urlPatterns={"/View/*"})
public class View extends HttpServlet {
    Cluster cluster = null;
    
    public void init(ServletConfig config) throws ServletException {
        cluster = Hosts.getCluster();
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getPathInfo().split("/")[1];
        Logger.getLogger(View.class).log(Level.INFO, id);  
        
        // check if image exists. if not, redirect to index.
        ResultSet exists = Hosts.query(cluster, "SELECT id FROM instagrim.images WHERE id = ?", id);
        if ((exists.all().isEmpty())) {
            request.getSession().setAttribute("error", "This image does not exist.");    
            response.sendRedirect("/Instagrim/Home"); 
            return;                        
        }
        
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/view.jsp?id=" + id);
	requestDispatcher.forward(request, response);        
    }
}
