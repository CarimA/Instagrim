package models;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import exceptions.*;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.Hosts;
import utils.Validation;

public class User {
    private Cluster cluster;
    public String username;
    public String displayUsername;
   
    private User(Cluster cluster, String username) {
        this.cluster = cluster;
        this.username = username.toLowerCase();
        this.displayUsername = username;
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public String getDisplayUsername() {
        return this.displayUsername;
    }
    
    public static User login(Cluster cluster, String username, String password) throws NoInformationException, UsernameInvalidException, UnsupportedEncodingException, NoSuchAlgorithmException, PasswordInvalidException {
        // make sure something's actually there...
        if ((username == null || password == null) ||
                (username.equals("") || password.equals(""))) {
            throw new NoInformationException();
        }        
        
        // check that username exists
        if (!exists(cluster, username)) {
            throw new UsernameInvalidException();
        }
     
        // check that password is correct
        Row r = Hosts.query(cluster, "SELECT displayUsername, password, salt FROM users WHERE username = ?", username.toLowerCase()).all().get(0);
        if (!Validation.validatePassword(password, r.getString("password"), r.getString("salt"))) {
            throw new PasswordInvalidException();
        }
        
        // correct username to their registered display name and return.
        return new User(cluster, r.getString("displayUsername"));
    }
    
    public static User register(Cluster cluster, String username, String password, String passwordConfirm) throws NoInformationException, UsernameTakenException, PasswordsDoNotMatchException, PasswordRequiresUppercaseException, PasswordRequiresLowercaseException, PasswordRequiresDigitException, PasswordRequiresSixCharactersException, UnsupportedEncodingException, NoSuchAlgorithmException, UsernameTooLongException {
        // make sure something's actually there...
        if ((username == null || password == null) ||
                (username.equals("") || password.equals(""))) {
            throw new NoInformationException();
        }
        
        // make sure username isn't taken.
        if (exists(cluster, username)) {
            throw new UsernameTakenException();
        }
        
        // make sure username isn't too long.
        if (username.length() > 14) {
            throw new UsernameTooLongException();
        }        
        
        // check some password requirements.
        // Check that the password and confirmation actually match.
        if (!password.equals(passwordConfirm)) {
            throw new PasswordsDoNotMatchException();
        }
        
        // Must have at least one uppercase letter.
        boolean hasUpperCase = !password.equals(password.toLowerCase());
        if (!hasUpperCase) {
            throw new PasswordRequiresUppercaseException();
        }
        
        // Must have at least one lowercase letter.
        boolean hasLowerCase = !password.equals(password.toUpperCase());
        if (!hasLowerCase) {
            throw new PasswordRequiresLowercaseException();
        }
        
        // Must have at least one digit.
        if (!password.matches(".*\\d+.*")) {
            throw new PasswordRequiresDigitException();
        }
        
        // Must be at least 6 characters in length.
        if (password.length() < 6) {
            throw new PasswordRequiresSixCharactersException();
        }

        // Generate a random salt (using UUIDv4)
        String salt = UUID.randomUUID().toString();
        String hashedPassword = Validation.hashPassword(password, salt);

        // todo: set creation date

        // add user to database
        Hosts.query(cluster, "INSERT INTO instagrim.users(username, displayUsername, password, salt) VALUES (?, ?, ?, ?)", username.toLowerCase(), username, hashedPassword, salt);
        
        return new User(cluster, username);
    }
    
    public static boolean exists(Cluster cluster, String username) {        
        // query if said user exists...
        ResultSet users = Hosts.query(cluster, "SELECT username FROM users WHERE username = ?", username.toLowerCase());
        
        // if the result set is empty, they clearly do not exist.
        return !(users.all().isEmpty());
    }
}
