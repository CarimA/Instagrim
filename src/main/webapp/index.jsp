<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper>
    <div class="row">
        <div id="spin" class="twelve columns">
            <i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i>
        </div>
        <div id="grid" class="twelve columns">
        </div>
        <script src="js/masonry.js"></script>
        <script>            
            jQuery(document).ready(function(){
                let elem = document.querySelector('#grid');
                window.pk = new Packery(elem, {
                    "itemSelector": '.grid-item',
                    "percentPosition": true
                });
                
                $.getJSON('Image', (input) => {
                    let imgCount = 0;
                    let getImage = (i) => {
                        if (input[i]) {
                            console.log('Loading image #' + i);
                            let id;
                            $.getJSON('Image?id=' + input[i], (imagedata) => {
                                if (imagedata && imagedata[0]) {
                                    let image = new Image();
                                    image.src = 'data:image/jpeg;base64,' + imagedata[0]; 
                                    image.className = "grid-item";
                                    let id = Math.random().toString(36).substring(2, 8);
                                    image.id = id;
                                    image.onload = () => {
                                        $('#' + id).wrap($('<a>',{
                                            href: 'View/' + input[i]
                                         }));
                                         
                                        $('#' + id).fadeIn(500);
                                        $('#spin').fadeOut(500);
                                        
                                        //imgCount++;
                                        //getImage(imgCount);

                                        // not sure why layout() isn't working as intended.
                                        // pk.layout();
                                        window.pk = new Packery(elem, {
                                            "itemSelector": '.grid-item',
                                            "percentPosition": true
                                        });
                                    };
                                    
                                    image.onerror = () => {
                                        //imgCount++;
                                        getImage(i);
                                        $('#' + image.id).remove();
                                    };
                                    
                                    $('#grid').prepend(image);
                                }
                            });
                        }
                    };
                    
                    for (let i = 0; i < input.length; i++) {
                        getImage(i);
                    }
                });
            });
        </script>
    </div>    
</t:wrapper>
