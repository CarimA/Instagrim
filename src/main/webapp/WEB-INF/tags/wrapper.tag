<%@tag description="Meta Information" pageEncoding="UTF-8"%>
<%@tag import="models.*" %>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <title>Instagrim</title>
        
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link rel="stylesheet" href="/Instagrim/css/normalize.css" />
        <link rel="stylesheet" href="/Instagrim/css/style.css" />
    </head>
    <body>        
        <script src="https://use.fontawesome.com/3701aeb528.js"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
        <div class="container">
            <header class="row">
                <div class="left">
                    <h1><a href="/Instagrim">Instagrim</a></h1>
                    <span>Your world in black & white!</span>
                </div>
                
                <nav class="right">
                    <ul>
                        <%
                            User user = (User)session.getAttribute("session");
                            if (user == null) {
                        %>
                            <li>
                                <a href="/Instagrim/Login" class="button no-border">login</a>
                            </li>
                            <li>
                                <a href="/Instagrim/Login" class="button button-primary"><i class="fa fa-camera" aria-hidden="true"></i> upload</a>
                            </li>
                        <%
                            } else {
                        %>
                            <li>
                                <a href="/Instagrim/Profile/${session.getDisplayUsername()}">Hello, ${session.getDisplayUsername()}</a>
                            </li>
                            <li>
                                <form id="formimageupload" method="POST" enctype="multipart/form-data" action="/Instagrim/Image" class="button button-primary" style="display: inline-block; margin: 0; position: relative;">
                                    <i class="fa fa-camera" aria-hidden="true"></i> Upload
                                    <input id="imageupload" name="img" type="file" accept="image/x-png, image/jpeg" style="margin: 0; padding: 0; opacity: 0; position: absolute; top: 0; bottom: 0; left: 0; right: 0;">
                                </form>
                            </li>
                            <li>
                                <a href="/Instagrim/Logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
                            </li>                        
                        <%
                            }                                
                        %>
                    </ul>
                </nav>
            </header>
                <hr>
                <%
                    String suc = (String)session.getAttribute("success");
                    if (suc != null) {
                        if (!suc.equals("")) {
                            // clear the flash error and display.                    
                            %>
                                <div class="row success">
                                    <p><i class="fa fa-lightbulb-o" aria-hidden="true"></i> <strong>${success}</strong></p>
                                    <a onclick="$('.success').fadeOut();"><i class="fa fa-times" aria-hidden="true"></i></a>
                                </div>
                            <%
                            session.setAttribute("success", null);
                        }
                    }

                    String err = (String)session.getAttribute("error");
                    if (err != null) {
                        if (!err.equals("")) {
                            // clear the flash error and display.                    
                            %>
                                <div class="row error">
                                    <p><i class="fa fa-bug" aria-hidden="true"></i> <strong>${error}</strong></p>
                                    <a onclick="$('.error').fadeOut();"><i class="fa fa-times" aria-hidden="true"></i></a>
                                </div>
                            <%
                            session.setAttribute("error", null);
                        }
                    }
                %>
            <jsp:doBody/>
            
            <footer class="row">
                
            </footer>
        </div>
           
        <div id="login" style="display: none;">
            <div class="login-box">
                <div class="row">
                    <div class="twelve columns">
                        <h1>Sign In to Instagrim</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="six columns">
                        <form method="POST" action="Login">
                            <input class="u-full-width" type="text" placeholder="Your Username" name="username" />
                            <input class="u-full-width" type="password" placeholder="Your Password" name="password" />
                            <input class="button-primary" type="submit" value="Sign In" />
                        </form>
                    </div>
                    <div class="six columns">
                        <p>Not a member yet? If you love all things photography or images, you should be! We'd love to see you join!</p>
                        <ul>
                            <li>Post your images for the world to see</li>
                            <li>Discover new images and trends</li>
                            <li>Converse with other picto-lovers</li>
                        </ul>
                        <p><strong>Your new world in black & white!</strong></p>
                        <a href="/Instagrim/Login#register" class="button button-primary">Sign Up</a>
                    </div>
                </div>
            </div>            
        </div>
            
        <div id="register" style="display: none;">
            <div class="register-box">
                <div class="row">
                    <div class="twelve columns">
                        <h1>Create an Account</h1>
                    </div>
                    <div class="twelve columns">
                        <form method="POST" action="Register">
                            <input class="u-full-width" type="text" placeholder="Your Desired Username" name="username" />
                            <input class="u-full-width" type="password" placeholder="Your Password" name="password" />
                            <input class="u-full-width" type="password" placeholder="Confirm Your Password" name="password2" />
                            <p>Your username cannot be longer than 15 characters. Your password must contain one uppercase letter, one lowercase letter, one number and be 6 or more characters in length.</p>
                            <p>By joining Instagrim, you agree to our <a href="#">Community Rules</a> and <a href="#">Terms of Services</a>.</p>
                            <input class="button-primary" type="submit" value="Sign me up!" />
                        </form>
                    </div>
                    <div class="twelve coluumns">
                            <a href="/Instagrim/Login">I already have an account!</a>
                    </div>
                </div>
            </div>            
        </div>
            
        <!--<div id="upload" style="display: none;">
            <div class="login-box">
                <div class="row">
                    <form style="margin: 0;">
                        <div id="upload-picture" class="six columns" style="position: relative;">
                            <div class="upload-box">
                                <div class="upload-wrapper">
                                    <i class="fa fa-upload" aria-hidden="true"></i>
                                    <p>Click here to upload a picture</p>
                                </div>
                            </div>
                            <input type="file" accept="image/*" id="image" name="input" style="margin: 0; padding: 0; opacity: 0; position: absolute; top: 0; bottom: 0; left: 0; right: 0;">
                        </div>               
                        <div id="upload-camera" class="six columns" style="position: relative;">
                            <div class="upload-box">
                                <div class="upload-wrapper">
                                    <i class="fa fa-camera" aria-hidden="true"></i>
                                    <p>Upload directly from Camera</p>
                                </div>
                            </div>
                            <input type="file" capture accept="image/*" id="image" name="cameraInput" style="margin: 0; padding: 0; opacity: 0; position: absolute; top: 0; bottom: 0; left: 0; right: 0;">
                        </div>
                    </form>
                </div>
        </div>-->            
        <script src="/Instagrim/js/jquery-modal.js" type="text/javascript" charset="utf-8"></script>
        <script>
            $(document).ready(() => {
                let imgup = document.querySelector('#imageupload');
                if (imgup) {
                    imgup.addEventListener('change', () => {
                        document.querySelector('#formimageupload').submit();
                    }, false);
                }
                
                $('a[href="/Instagrim/Login"]').click((e) => {
                    // waaaait, if their screen is smaller than 550px in width, carry on to the login page.
                    // no fancy JS required!
                    let width = $(window).width();
                    if (width >= 550) {
                        e.preventDefault();
                        $('#login').modal({
                            fadeDuration: 600,
                            fadeDelay: 0.50,
                            showClose: false
                        });                        
                    }
                });     
                $('a[href="/Instagrim/Login#register"]').click((e) => {
                    // this modal can't be reached without the previous one, so no checks needed here.
                    e.preventDefault();
                    $('#register').modal({
                        fadeDuration: 600,
                        fadeDelay: 0.50,
                        showClose: false
                    });
                });
                
                /*$('a[href="upload.jsp"]').click((e) => {
                   // more mobile checks: we don't want a modal if it's going to intrude.
                   let width = $(window).width();
                   if (width >= 550) {
                       e.preventDefault();
                       $('#upload').modal({
                           fadeDuration: 600,
                           fadeDelay: 0.50,
                           showClose: false
                        });
                    }
                });*/
            });
        </script>
    </body>
</html>