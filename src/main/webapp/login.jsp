<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper>
    <div class="row">
        <div class="six columns">
            <h2>Log In to Instagrim</h2>
            <form method="POST" action="Login">
                <input class="u-full-width" type="text" placeholder="Your Username" name="username" />
                <input class="u-full-width" type="password" placeholder="Your Password" name="password" />
                <input class="button-primary" type="submit" value="Login" />
            </form>
        </div>
        <div class="six columns">
            <h2>Create an Account</h2>
            <form method="POST" action="Register">
                <input class="u-full-width" type="text" placeholder="Your Desired Username" name="username" />
                <input class="u-full-width" type="password" placeholder="Your Password" name="password" />
                <input class="u-full-width" type="password" placeholder="Confirm Your Password" name="password2" />
                <p>Your username cannot be longer than 15 characters. Your password must contain one uppercase letter, one lowercase letter, one number and be 6 or more characters in length.</p>
                <p>By joining Instagrim, you agree to our <a href="#">Community Rules</a> and <a href="#">Terms of Services</a>.</p>
                <input class="button-primary" type="submit" value="Register" />
            </form>
        </div>
    </div>
</t:wrapper>
