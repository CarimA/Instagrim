<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper>
    <div class="row">
        <div id="grid" class="twelve columns">
            
        </div>
        <div class="twelve columns">
            <p id="details"></p>
        </div>        
        
        <script>            
            jQuery(document).ready(function(){      
                $.getJSON('/Instagrim/Image?id=' + window.location.pathname.split('/')[3], (imagedata) => {
                    //if (imagedata && imagedata[0]) {
                        let image = new Image();
                        image.src = 'data:image/jpeg;base64,' + imagedata[0]; 
                        image.className = 'wide-display';

                        $('#grid').prepend(image);
                        
                        $('#details').html('Uploaded by <a href="/Instagrim/Profile/' + imagedata[1] + '">' + imagedata[1] + '</a> at ' + new Date(imagedata[2]));
                    //};
                });
            });
        </script>
    </div>    
</t:wrapper>
